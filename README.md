Has the card holder from your Codenames or Codenames Duet game gone
missing or broken? Print a new one!

Designed using FreeCAD Link Stage 3 branch.

I found that printing it with a small layer height so that card
holder tangs are not too rough on the card edges worked better than
printing it on its side. I used PETG and 0.15mm layers.

Trivially different from the holder shipped with the game to
optimize for 3D printing, but meant to be the same size and
envelope in order to fit in any organizers created for the
game.

CC0 no rights reserved.
